# monero-native
> native bindings for monero

### Requirements

* Node 8.x for [N-API](https://nodejs.org/api/n-api.html)
* ...

### Examples

##### CryptoNight

```js
const moneroNative = require('monero-native')
moneroNative.slowHash(Buffer.from('This is a test', 'ascii'), 0, (err, buf) => {
  if (err) return console.log(err)
  console.log(buf.toString('hex')) // 'a084f01d1437...'
})
```

##### ConvertBlob

```js
const moneroNative = require('monero-native')
moneroNative.convertBlob(input, (err, buf) => {
  if (err) return console.log(err)
  console.log(buf.toString('hex'))
})
```

### License

Released under the terms of the 3-Clause BSD license. See `LICENSE.txt` for more
information.
