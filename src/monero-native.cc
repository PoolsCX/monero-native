#include <napi.h>

#include "ConvertBlob.h"
#include "SlowHash.h"

Napi::Object Init(Napi::Env env, Napi::Object exports) {
  exports.Set(Napi::String::New(env, "convertBlob"),
              Napi::Function::New(env, ConvertBlob));
  exports.Set(Napi::String::New(env, "slowHash"),
              Napi::Function::New(env, SlowHash));
  return exports;
}

NODE_API_MODULE(moneroNative, Init)
