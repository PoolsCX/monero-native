#include <string>
#include <napi.h>

#include <cryptonote_basic/cryptonote_format_utils.h>

#include "ConvertBlob.h"

class ConvertBlobAsyncWorker : public Napi::AsyncWorker {
public:
  ConvertBlobAsyncWorker(
    const Napi::Function &callback,
    const char *in,
    size_t inLen
  ) : Napi::AsyncWorker(callback), in(in), inLen(inLen)
  {}

protected:
  void Execute() override {
    std::string input(in, inLen);
    cryptonote::block bl;
    if (!cryptonote::parse_and_validate_block_from_blob(input, bl)) {
      Napi::Error::New(Env(), "Unable to parse block").ThrowAsJavaScriptException();
      return;
    }
    output = cryptonote::get_block_hashing_blob(bl);
  }

  void OnOK() override {
    Callback().MakeCallback(
      Receiver().Value(),
      {
        Env().Null(),
        Napi::Buffer<char>::Copy(Env(), output.data(), output.size())
      }
    );
  }

private:
  const char *in;
  size_t inLen;
  std::string output;
};

Napi::Value ConvertBlob(const Napi::CallbackInfo& info) {
  Napi::Buffer<const char> in = info[0].As<Napi::Buffer<const char>>();
  Napi::Function cb = info[1].As<Napi::Function>();

  (new ConvertBlobAsyncWorker(cb, in.Data(), in.Length()))->Queue();

  return info.Env().Undefined();
}
