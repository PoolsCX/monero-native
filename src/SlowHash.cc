#include <crypto/hash.h>
#include <napi.h>

#include "SlowHash.h"

class SlowHashAsyncWorker : public Napi::AsyncWorker {
public:
  SlowHashAsyncWorker(
    const Napi::Function &callback,
    const char *in,
    size_t inLen,
    int variant
  ) : Napi::AsyncWorker(callback), in(in), inLen(inLen), variant(variant)
  {}

protected:
  void Execute() override {
    crypto::cn_slow_hash(in, inLen, out, variant);
  }

  void OnOK() override {
    Callback().MakeCallback(
      Receiver().Value(),
      {
        Env().Null(),
        Napi::Buffer<char>::Copy(Env(), out.data, crypto::HASH_SIZE)
      }
    );
  }

private:
  const char *in;
  size_t inLen;
  int variant;
  crypto::hash out;
};

Napi::Value SlowHash(const Napi::CallbackInfo &info) {
  Napi::Buffer<const char> in = info[0].As<Napi::Buffer<const char>>();
  int variant = info[1].As<Napi::Number>().Int32Value();
  Napi::Function cb = info[2].As<Napi::Function>();

  (new SlowHashAsyncWorker(cb, in.Data(), in.Length(), variant))->Queue();

  return info.Env().Undefined();
}
