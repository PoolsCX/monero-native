const moneroNative = require('./')

describe('CryptoNight slow hash', () => {
  describe('variant 0', () => {
    test('case 0', done => {
      expect.assertions(1)
      moneroNative.slowHash(Buffer.alloc(0), 0, (err, buf) => {
        if (err) return done(err)
        expect(buf.toString('hex')).toEqual('eb14e8a833fac6fe9a43b57b336789c46ffe93f2868452240720607b14387e11')
        done()
      })
    })
    test('case 1', done => {
      expect.assertions(1)
      moneroNative.slowHash(Buffer.from('This is a test', 'ascii'), 0, (err, buf) => {
        if (err) return done(err)
        expect(buf.toString('hex')).toEqual('a084f01d1437a09c6985401b60d43554ae105802c5f5d8a9b3253649c0be6605')
        done()
      })
    })
  })
  describe('variant 1', () => {
    test('case 0', done => {
      expect.assertions(1)
      moneroNative.slowHash(Buffer.from('00000000000000000000000000000000000000000000000000000000000000000000000000000000000000', 'hex'), 1, (err, buf) => {
        if (err) return done(err)
        expect(buf.toString('hex')).toEqual('b5a7f63abb94d07d1a6445c36c07c7e8327fe61b1647e391b4c7edae5de57a3d')
        done()
      })
    })
    test('case 1', done => {
      expect.assertions(1)
      moneroNative.slowHash(Buffer.from('38274c97c45a172cfc97679870422e3a1ab0784960c60514d816271415c306ee3a3ed1a77e31f6a885c3cb', 'hex'), 1, (err, buf) => {
        if (err) return done(err)
        expect(buf.toString('hex')).toEqual('ed082e49dbd5bbe34a3726a0d1dad981146062b39d36d62c71eb1ed8ab49459b')
        done()
      })
    })
  })
})

test('ConvertBlob', done => {
  expect.assertions(1)
  moneroNative.convertBlob(Buffer.from('0707d7dcf0d8054493ab0d8b6599c3d11c23e90e69598c1fef2d8513d27a09dd06a0a90342d8b00000000002da916101ff9e916101cfbae6a7b78301028b9115963e93c44da6b37358562409649aff1b14f6d8a9674271e93595e06f855f01a2be60f21770b0185ff47bcb4627844deb7f5d5a6eb1ca293e0d70bf43fd568d023c000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b80a7a97cff1ef670f535d6087f724ea7b4b3cc37098eac95682d724e90cea007d2c1a8389564d3890c30431404366f8e2f3ee491f1dcbfecd84a5cdeb1d724ae308fb17cc5ed29e6753e8f6794c19a1da0a0d9e9eedbad6408c0c19f7748002b034796271efbf13b677849c3e1392e5aeda959c3bf752e4ebc930b2e3f25888bc60d8047622f2976875bab7939f899bc89e3dcaac785529dc4fb3dc39e0f188f9b2bcd6127cf7c3c1b465f4fb306ba564e972c763905410f2e127334fb69665fd7883c44e7cd4c58fe529dce54db53d652315ec5fc62c56699d3e860d2a598a913cad6ad38072dac718ea4532de5ff535cd6d63ee9d80955ea8a1899f57f3a9d42bb4454e94499904e515117be0720cf304c9a3e3927ae7b62f6e2be18c7e8ed77aa8bccfe0b156d3fb654527d1a511b8cf0bfed99b6439465a829dee9d4271d60f7d124cddb8a69b151328dd083249e20341b805602b9619e6a55e8c85bb203', 'hex'), (err, res) => {
    if (err) return done(err)
    expect(res.toString('hex')).toEqual('0707d7dcf0d8054493ab0d8b6599c3d11c23e90e69598c1fef2d8513d27a09dd06a0a90342d8b000000000bd42cdba19216661312de8c4938791bfc1af790a24b7013dbf163e91b7f121a30c')
    done()
  })
})
