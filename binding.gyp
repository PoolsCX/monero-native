{
  "targets": [
    {
      "target_name": "monero-native",
      "cflags": [
        "-fexceptions",
        "-maes"
      ],
      "cflags_cc": [
        "-fexceptions",
        "-frtti",
        "-maes"
      ],
      "link_settings": {
        "libraries": [
          "-lboost_system"
        ]
      },
      "include_dirs": [
        "<!@(node -p \"require('node-addon-api').include\")",
        "third-party/monero/src",
        "third-party/monero/contrib/epee/include",
        "third-party/monero/external/easylogging++"
      ],
      "defines": [
        "NAPI_CPP_EXCEPTIONS",
        "AUTO_INITIALIZE_EASYLOGGINGPP"
      ],
      "sources": [
        "src/ConvertBlob.cc",
        "src/SlowHash.cc",
        "src/monero-native.cc",
        "third-party/monero/contrib/epee/src/memwipe.c",
        "third-party/monero/external/easylogging++/easylogging++.cc",
        "third-party/monero/src/crypto/groestl.c",
        "third-party/monero/src/crypto/hash-extra-blake.c",
        "third-party/monero/src/crypto/hash-extra-groestl.c",
        "third-party/monero/src/crypto/hash-extra-jh.c",
        "third-party/monero/src/crypto/hash-extra-skein.c",
        "third-party/monero/src/crypto/hash.c",
        "third-party/monero/src/crypto/keccak.c",
        "third-party/monero/src/crypto/skein.c",
        "third-party/monero/src/crypto/slow-hash.c",
        "third-party/monero/src/crypto/tree-hash.c",
        "third-party/monero/src/cryptonote_basic/cryptonote_format_utils.cpp"
      ]
    }
  ]
}
